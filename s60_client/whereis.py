#===============================================================================
# Whereis for Python S60
#
# Send current location to a webserver via XMLRPC.
#===============================================================================
import location, positioning, appuifw, e32, key_codes, graphics, xmlrpclib, e32dbm

#===============================================================================
# Variables and constants
#===============================================================================
# Standard color values.
WHITE = (255,255,255)
BLUE = (0,0,255)

# This file stores the application settings: username, pass and URL.
DBFILE = u"c:\\data\\python\\whereis_settings.db"

# Global variables store the current latitude and longitude.
lat = 0.0;
long = 0.0;

# Message for user interface.
message = u"Welcome to Whereis."

#===============================================================================
# Functions
#===============================================================================

"""
" Load settings from DB, ask for new username and save if entered.
"""
def set_username():
	settings = load_settings()
	username = appuifw.query(u"Enter username:", "text", unicode(settings["username"]))
	if username:
		write_db(u"username", username)
		appuifw.note(u"Username saved")

"""
" Ask for new password and save if entered. We use plain text because
" typing passwords that get starred **** out always makes it very difficult
" in my opinion. If you're worried about people looking over your shoulder,
" you've got bigger problems than stolen passwords.
"""
def set_password():
	password = appuifw.query(u"Enter password:", "text")
	if password:
		write_db(u"password", password)
		appuifw.note(u"Password saved")

"""
" Load settings from DB, as for new node id, save if entered.
"""
def set_nid():
	settings = load_settings()
	nid = appuifw.query(u"Enter node id:", "text", unicode(settings["nid"]))
	if nid:
		write_db(u"nid", nid)
		appuifw.note(u"Node id saved")

"""
" Load settings from DB, ask for new url and save if entered.
"""
def set_url():
	settings = load_settings()
	url = appuifw.query(u"Enter url:", "text", unicode(settings["url"]))
	if url:
		write_db(u"url", url)
		appuifw.note(u"Url saved")

"""
" Write a key/val pair to the database file.
"""
def write_db(key, val):
	db = e32dbm.open(DBFILE, "cf")
	db[key] = val
	db.close()

"""
" Load all settings, and return them as a dictionary.
"""
def load_settings():
	db = e32dbm.open(DBFILE, "cr")
	settings = { u"url" : u"", u"username" : u"", u"password" : u"", u"nid" : u"" }
	for key, value in db.items():
		settings[key] = value
	db.close()
	return settings

"""
" Upload current position to server using XMLRPC.
"""
def sync():
	global lat, long, message
	positioning.stop_position()
	settings = load_settings()
	message = u"Attempting to sync"
	draw_ui()
	serverProxy = xmlrpclib.ServerProxy(settings["url"])
	# serverProxy = xmlrpclib.ServerProxy("http://192.168.5.96:8888/custom/whereis/www/xmlrpc.php");
	result = serverProxy.whereis.updatePosition(int(settings["nid"]), lat, long)
	message = unicode(result)
	draw_ui()
	appuifw.note(unicode(result))
	init_gps()

"""
" Start positioning. it runs in a thread and calls "handle_pos" callback
" every second with latest info.
"""
def init_gps():
	positioning.set_requestors([{"type":"service","format":"application","data":"test_app"}])
	positioning.position(course = 0, satellites = 1, callback = handle_pos, interval = 1000000, partial = 1)

def handle_pos(data):
	global lat, long
	lat = data["position"]["latitude"]
	long = data["position"]["longitude"]
	draw_ui()

"""
" Double buffered user interface. We draw to the img first, then call
" handle_redraw to actually blit the image to the canvas. This is
" useful because popups obscure the canvas and we want to quickly redraw
" once they dissappear.
"""
def draw_ui():
	global lat, long, message
	img.clear(BLUE)
	ulat = unicode(lat)
	ulong = unicode(long)
	# nan is infinity, we override with "searching" - a more relevant message.
	if ulat == u"nan":
		ulat = u"<< searching >>"
	if ulong == u"nan":
		ulong = u"<< searching >>"
	img.text((20,50), u"Lat: " + ulat, fill = WHITE)
	img.text((20,70), u"Long: " + ulong, fill = WHITE)
	img.text((30,150), message, fill = WHITE)
	handle_redraw(None)

def handle_redraw(rect):
	if img:
		canvas.blit(img)

"""
" Doesn't do much at the moment, but could handle user keypresses.
"""
def handle_event(event):
	ev = event["keycode"]

def quit():
	positioning.stop_position()
	app_lock.signal()

#===============================================================================
# Main program
#===============================================================================

# Output execution begin and end points, useful for debugging.
print "START WHEREIS ==>"

# Set up canvas of application.
img = None
canvas = appuifw.Canvas(redraw_callback = handle_redraw, event_callback = handle_event)
appuifw.app.body = canvas

# Set the various application parameters.
appuifw.app.exit_key_handler = quit
appuifw.app.title = u"Whereis"
appuifw.app.screen = "large"

# Set up the main menu on the left "select" key.
menu_sync = (u"Sync to server", sync)
menu_set_username = (u"Username", set_username)
menu_set_password = (u"Password", set_password)
menu_set_url = (u"URL", set_url)
menu_set_nid = (u"Node id", set_nid)
menu_configure = (u"Config", (menu_set_username, menu_set_password, menu_set_url, menu_set_nid))
appuifw.app.menu = [menu_sync, menu_configure]

# Set up image to be the same size as canvas for double buffering.
w, h = canvas.size
img = graphics.Image.new((w, h))
img.clear(BLUE)

# Start the execution!
draw_ui()
init_gps()
app_lock = e32.Ao_lock()
app_lock.wait()

# When exiting, mark the end for debugging purposes in console.
print "<== EXIT WHEREIS"
